\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lintek}
\LoadClass{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[swedish]{babel}
\RequirePackage{fourier}
\RequirePackage{fullpage}
\RequirePackage{fancyhdr}
\RequirePackage[yyyymmdd]{datetime}
\RequirePackage{lastpage}
\RequirePackage{graphicx}
\RequirePackage{xcolor}
\RequirePackage{enumerate}
\RequirePackage{amsmath}
\RequirePackage{csquotes}
\AtBeginEnvironment{quote}{\itshape}

\setlength\parindent{0pt}
\pagestyle{fancy}
\renewcommand{\dateseparator}{--}

\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%\setlength\headheight{53.15198pt}
\RequirePackage[margin=2.5cm,headheight=60pt,includeheadfoot]{geometry}

\lhead{\includegraphics[width=5cm]{lintek.png}\\}
\rhead{\today\\\thepage\:(\pageref{LastPage})\\}
\cfoot{{\small
\textbf{Postadress} LinTek, Tekniska högskolan, 581 83 Linköping
\textbf{Besöksadress} Tekniska högskolan, Kårallen, plan 3\\
\textbf{Telefon} 070-269 45 82
\textbf{Org.nr} 822001-0683
\textbf{Bankgiro} 515-1493
\textbf{E-post} uas@lintek.liu.se \textbf{Hemsida} www.lintek.liu.se}}

\makeatletter
\def\@maketitle{
 \newpage
 {\huge Rapportmall för studeranderepresentanter}

 \vspace{2cm}

 {\Large
 \def\arraystretch{1.5}
 \setlength\tabcolsep{3em}
 \begin{tabular}{ l l }
   \textbf{Grupp:}      & \@title  \\
   \textbf{Datum:}      & \@date   \\
   \textbf{Författare:} & \@author \\
 \end{tabular}
 }

 \newpage
}
\makeatother

\makeatletter
\renewcommand{\@seccntformat}[1]{}
\makeatother
