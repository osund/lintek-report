MAIN=lintek-rapport

all:
	latexmk $(MAIN).tex

watch:
	latexmk $(MAIN).tex -pvc -view=none

clean:
	latexmk -c
	rm -f $(MAIN).pdf $(MAIN)-markdown.tex

